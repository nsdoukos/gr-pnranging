# gr-pnranging

The `gr-pnranging` GNU Radio out-of-tree (OOT) module provides a set of Code
Division Multiple Access (CDMA) Direct Sequence (DS) spread spectrum modulation
schemes for CCSDS Users. The codes are pseudo random and have been specifically
chosen to limit the signal Power Spectral Density (PSD) at the Earth's surface
to minimize Multiple Access Interference (MAI).

The implementation follows the specifications described in the CCSDS blue books
[CCSDS 414.1-B-2](https://public.ccsds.org/Pubs/414x1b2.pdf)
[CCSDS 415.1-B-1](https://public.ccsds.org/Pubs/415x1b1.pdf)

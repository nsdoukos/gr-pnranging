/* -*- c++ -*- */
/*
 * Copyright 2021 Libre Space Foundation.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <cstdint>
#include <string>
#include <vector>

class lfsr
{
private:
    uint32_t d_taps;
    uint8_t d_stages;
    uint8_t d_number_taps;
    uint32_t d_maximal_length;
    uint32_t d_lfsr;
    uint8_t d_last_out;
    std::vector<uint8_t> d_k_last_out;

    uint8_t count_bits(uint16_t number);
    uint8_t odd_parity(uint8_t number);
    uint32_t validate_taps(std::string taps);
    void update_out(uint8_t &out);

public:
    lfsr() = default;
    lfsr(std::string seed, std::string taps);
    lfsr(std::string seed, std::string taps, uint8_t stages);

    void init_lfsr(std::string seed, std::string taps);
    void init_lfsr(std::string seed, std::string taps, uint8_t stages);

    uint8_t next(void);
    std::vector<uint8_t> next_k(int k);
    std::vector<uint8_t> generate_maximal_length();
    uint8_t next_xor_stage(uint8_t stage);
    std::vector<uint8_t> next_k_xor_stage(int k, uint8_t stage);
    std::vector<uint8_t> generate_maximal_length_xor_stage(uint8_t stage);

    uint8_t get_stages(void) const;
    uint32_t get_maximal_length(void) const;


    void print_lfsr_state(void);
    void print_all(void);

};

/* -*- c++ -*- */
/*
 * Copyright 2021 Libre Space Foundation.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_PNRANGING_DSSS_NETWORK_RECEIVER_H
#define INCLUDED_PNRANGING_DSSS_NETWORK_RECEIVER_H

#include <pnranging/api.h>
#include <gnuradio/sync_block.h>
// #include <pmt/pmt.h>

namespace gr {
  namespace pnranging {

    /*!
     * \brief <+description of block+>
     * \ingroup pnranging
     *
     */
    class PNRANGING_API dsss_network_receiver : virtual public gr::sync_block
    {
     public:
      typedef std::shared_ptr<dsss_network_receiver> sptr;

      /*!
       * \brief Return a shared_ptr to a new instance of pnranging::dsss_network_receiver.
       *
       * To avoid accidental use of raw pointers, pnranging::dsss_network_receiver's
       * constructor is in a private implementation
       * class. pnranging::dsss_network_receiver::make is the public interface for
       * creating new instances.
       */
      static sptr make();
    };

  } // namespace pnranging
} // namespace gr

#endif /* INCLUDED_PNRANGING_DSSS_NETWORK_RECEIVER_H */

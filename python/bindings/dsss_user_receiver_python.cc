/*
 * Copyright 2021 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(dsss_user_receiver.h)                                        */
/* BINDTOOL_HEADER_FILE_HASH(3f6f9da03e6d6c39010e034ddab71c38)                     */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <pnranging/dsss_user_receiver.h>
// pydoc.h is automatically generated in the build directory
#include <dsss_user_receiver_pydoc.h>

void bind_dsss_user_receiver(py::module& m)
{

    using dsss_user_receiver    = gr::pnranging::dsss_user_receiver;


    py::class_<dsss_user_receiver, gr::sync_block, gr::block, gr::basic_block,
        std::shared_ptr<dsss_user_receiver>>(m, "dsss_user_receiver", D(dsss_user_receiver))

        .def(py::init(&dsss_user_receiver::make),
           D(dsss_user_receiver,make)
        )
        



        ;




}









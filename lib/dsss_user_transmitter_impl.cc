/* -*- c++ -*- */
/*
 * Copyright 2021 Libre Space Foundation.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "dsss_user_transmitter_impl.h"
#include <cmath>

namespace gr {
  namespace pnranging {

    using output_type = gr_complex;
    dsss_user_transmitter::sptr
    dsss_user_transmitter::make()
    {
      return gnuradio::make_block_sptr<dsss_user_transmitter_impl>(
        );
    }


    /*
     * The private constructor
     */
     dsss_user_transmitter_impl::dsss_user_transmitter_impl()
       : gr::sync_block("dsss_user_transmitter",
               gr::io_signature::make(0, 0, 0),
               gr::io_signature::make(1, 1, sizeof(output_type))),
        d_mode(2)
    {
        switch (d_mode) {
            case 1:
                d_return_command_ch_register.
                    init_lfsr("111111111111111111", "000001000000000001");

                d_return_command_ch_m_sequence =
                    d_return_command_ch_register.
                        generate_maximal_length();
                d_return_range_ch_m_sequence =
                    d_return_range_ch_register.
                        generate_maximal_length_xor_stage(9);
                break;
            case 2:
                d_return_command_ch_register.
                    init_lfsr("10000000000", "01000000001");
                d_non_coherent_fixed_register.
                    init_lfsr("00000000001", "10101000001");
                d_return_range_ch_register.
                    init_lfsr("10000000000", "01001001001");

                d_return_command_ch_m_sequence =
                    d_return_command_ch_register.generate_maximal_length();
                d_return_fixed_ch_m_sequence =
                    d_non_coherent_fixed_register.generate_maximal_length();
                d_return_range_ch_m_sequence =
                    d_return_range_ch_register.generate_maximal_length();

                break;
            case 3:
                d_return_command_ch_register.
                    init_lfsr("111111111111111111", "000001000000000001");

                d_return_command_ch_m_sequence =
                    d_return_command_ch_register.
                        generate_maximal_length();
                break;
            default:
                std::cout << "ERROR: No such DG1 mode" << std::endl;
                break;
        }
    }

    /*
     * Our virtual destructor.
     */
    dsss_user_transmitter_impl::~dsss_user_transmitter_impl()
    {
    }

    int
    dsss_user_transmitter_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      output_type *out = reinterpret_cast<output_type*>(output_items[0]);

      switch(d_mode) {
          case 1:
            for (auto i = 0; i < d_return_range_ch_m_sequence.size(); i++) {
                out[i] = d_return_range_ch_m_sequence[i];
            }
            break;
          // Non-coherent return mode
          case 2:
            for (auto i = 0; i < d_return_range_ch_m_sequence.size(); i++) {
                out[i] = d_return_fixed_ch_m_sequence[i] ^
                         d_return_range_ch_m_sequence[i];
            }
            break;
          case 3:
            break;
      }
      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return d_return_range_ch_register.get_maximal_length();
    }

  } /* namespace pnranging */
} /* namespace gr */

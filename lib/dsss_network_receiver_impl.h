/* -*- c++ -*- */
/*
 * Copyright 2021 Libre Space Foundation.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_PNRANGING_DSSS_NETWORK_RECEIVER_IMPL_H
#define INCLUDED_PNRANGING_DSSS_NETWORK_RECEIVER_IMPL_H

#include <pnranging/dsss_network_receiver.h>

namespace gr {
  namespace pnranging {

    class dsss_network_receiver_impl : public dsss_network_receiver
    {
     private:
      // Nothing to declare in this block.

     public:
      dsss_network_receiver_impl();
      ~dsss_network_receiver_impl();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );
    };

  } // namespace pnranging
} // namespace gr

#endif /* INCLUDED_PNRANGING_DSSS_NETWORK_RECEIVER_IMPL_H */


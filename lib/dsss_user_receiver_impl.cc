/* -*- c++ -*- */
/*
 * Copyright 2021 Libre Space Foundation.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include "dsss_user_receiver_impl.h"

namespace gr {
  namespace pnranging {

    using input_type = gr_complex;
    dsss_user_receiver::sptr
    dsss_user_receiver::make()
    {
      return gnuradio::make_block_sptr<dsss_user_receiver_impl>(
        );
    }


    /*
     * The private constructor
     */
    dsss_user_receiver_impl::dsss_user_receiver_impl()
      : gr::sync_block("dsss_user_receiver",
              gr::io_signature::make(1, 1, sizeof(input_type)),
              gr::io_signature::make(0, 0, 0))
    {}

    /*
     * Our virtual destructor.
     */
    dsss_user_receiver_impl::~dsss_user_receiver_impl()
    {
    }

    int
    dsss_user_receiver_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      const input_type *in = reinterpret_cast<const input_type*>(input_items[0]);

      #pragma message("Implement the signal processing in your block and remove this warning")
      // Do <+signal processing+>

      // Tell runtime system how many output items we produced.
      return noutput_items;
    }

  } /* namespace pnranging */
} /* namespace gr */

/* -*- c++ -*- */
/*
 * Copyright 2021 Libre Space Foundation.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_PNRANGING_DSSS_USER_TRANSMITTER_IMPL_H
#define INCLUDED_PNRANGING_DSSS_USER_TRANSMITTER_IMPL_H

#include <pnranging/dsss_user_transmitter.h>
#include <pnranging/lfsr.h>

namespace gr {
  namespace pnranging {

    class dsss_user_transmitter_impl : public dsss_user_transmitter
    {
     private:
         lfsr d_return_command_ch_register;
         lfsr d_non_coherent_fixed_register;
         lfsr d_return_range_ch_register;

         std::vector<uint8_t> d_return_command_ch_m_sequence;
         std::vector<uint8_t> d_return_fixed_ch_m_sequence;
         std::vector<uint8_t> d_return_range_ch_m_sequence;

         int d_mode;
     public:
      dsss_user_transmitter_impl();
      ~dsss_user_transmitter_impl();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );

      gr_complex generate_symbols(std::vector<uint8_t> sequence);
    };

  } // namespace pnranging
} // namespace gr

#endif /* INCLUDED_PNRANGING_DSSS_USER_TRANSMITTER_IMPL_H */

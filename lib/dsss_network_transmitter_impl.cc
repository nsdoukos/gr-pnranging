/* -*- c++ -*- */
/*
 * Copyright 2021 Libre Space Foundation.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include <gnuradio/digital/constellation.h>
#include "dsss_network_transmitter_impl.h"
#include <cmath>

namespace gr {
  namespace pnranging {

    using output_type = uint8_t;
    dsss_network_transmitter::sptr
    dsss_network_transmitter::make()
    {
      return gnuradio::make_block_sptr<dsss_network_transmitter_impl>(
        );
    }


    /*
     * The private constructor
     */
    dsss_network_transmitter_impl::dsss_network_transmitter_impl()
      : gr::sync_block("dsss_network_transmitter",
              gr::io_signature::make(0, 0, 0),
              gr::io_signature::make(1 , 1, sizeof(output_type))),
              d_forward_command_ch_register({"0000000001", "0110010111"}),
              d_forward_range_ch_register({"111111111111111111", "000001000000000001"}),
              d_forward_command_fixed_register({"1001001000", "1110110001"}),
              d_curr_len(0)
    {
        /* Generate maximal length sequence for forward command channel */
        d_fwd_command_ch_m_sequence =
            d_forward_command_ch_register.generate_maximal_length();
        d_fwd_fixed_m_sequence =
            d_forward_command_fixed_register.generate_maximal_length();

        /* Generate maximal length sequence for forward ranging channel */
        d_fwd_range_ch_m_sequence =
            d_forward_range_ch_register.generate_maximal_length();

        message_port_register_in(pmt::intern("command"));
        set_msg_handler(pmt::intern("command"),
            [this](pmt::pmt_t command) { this->command_handler(command); });
    }

    /*
     * Our virtual destructor.
     */
    dsss_network_transmitter_impl::~dsss_network_transmitter_impl()
    {
    }

    int
    dsss_network_transmitter_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {
      output_type *out = reinterpret_cast<output_type*>(output_items[0]);

      for (auto i = 0; i < d_fwd_range_ch_m_sequence.size(); i++) {
          out[i] = d_fwd_range_ch_m_sequence[i];
      }

      return d_forward_range_ch_register.get_maximal_length();
      // return noutput_items;
    }

    void
    dsss_network_transmitter_impl::command_handler(pmt::pmt_t command)
    {
    }

  } /* namespace pnranging */
} /* namespace gr */

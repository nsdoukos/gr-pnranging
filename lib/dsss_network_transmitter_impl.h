/* -*- c++ -*- */
/*
 * Copyright 2021 Libre Space Foundation.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_PNRANGING_DSSS_NETWORK_TRANSMITTER_IMPL_H
#define INCLUDED_PNRANGING_DSSS_NETWORK_TRANSMITTER_IMPL_H

#include <pnranging/dsss_network_transmitter.h>
#include <pnranging/lfsr.h>

namespace gr {
  namespace pnranging {

    class dsss_network_transmitter_impl : public dsss_network_transmitter
    {
     private:
         lfsr d_forward_command_ch_register;
         lfsr d_forward_range_ch_register;
         lfsr d_forward_command_fixed_register;

         std::vector<uint8_t> d_fwd_command_ch_m_sequence;
         std::vector<uint8_t> d_fwd_fixed_m_sequence;
         std::vector<uint8_t> d_fwd_range_ch_m_sequence;

         pmt::pmt_t d_curr_command_vec;
         size_t d_curr_len;
     public:
      dsss_network_transmitter_impl();
      ~dsss_network_transmitter_impl();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );

      void command_handler(pmt::pmt_t command);
    };

  } // namespace pnranging
} // namespace gr

#endif /* INCLUDED_PNRANGING_DSSS_NETWORK_TRANSMITTER_IMPL_H */

/* -*- c++ -*- */
/*
 * Copyright 2021 Libre Space Foundation.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "pnranging/lfsr.h"
#include <iostream>
#include <bitset>
#include <cmath>

lfsr::lfsr(std::string seed, std::string taps) :
    d_taps(validate_taps(taps)),
    d_stages(seed.length()),
    d_number_taps(count_bits(d_taps)),
    d_maximal_length(pow(2, d_stages) -1),
    d_lfsr(std::stoul(seed, 0, 2)),
    d_last_out(0)
{
    // print_all();
}

lfsr::lfsr(std::string seed, std::string taps, uint8_t stages) :
    d_taps(validate_taps(taps)),
    d_stages(stages),
    d_number_taps(count_bits(d_taps)),
    d_maximal_length(pow(2, d_stages) - 1),
    d_lfsr(std::stoul(seed, 0, 2)),
    d_last_out(0)
{
    // print_all();
}

void
lfsr::init_lfsr(std::string seed, std::string taps)
{
    this->d_taps = validate_taps(taps);
    this->d_stages = seed.length();
    this->d_number_taps = count_bits(d_taps);
    this->d_maximal_length = pow(2, d_stages) - 1;
    this->d_lfsr = stoul(seed, 0, 2);
    this->d_last_out = 0;
}

void
lfsr::init_lfsr(std::string seed, std::string taps, uint8_t stages)
{
    this->d_taps = validate_taps(taps);
    this->d_stages = stages;
    this->d_number_taps = count_bits(d_taps);
    this->d_maximal_length = pow(2, d_stages) - 1;
    this->d_lfsr = stoul(seed, 0, 2);
    this->d_last_out = 0;
}

uint8_t
lfsr::next(void)
{
    uint8_t return_bit = 0;
    uint32_t tmp_xor = 0;
    tmp_xor = d_taps & d_lfsr;
    tmp_xor = odd_parity(tmp_xor);
    return_bit |= d_lfsr & 1;
    d_last_out = return_bit;
    d_lfsr >>= 1;
    d_lfsr |= (tmp_xor << (d_stages - 1));

    // print_lfsr_state();

    return return_bit;
}

std::vector<uint8_t>
lfsr::next_k(int k)
{
    uint8_t tmp = 0;
    int divisor = k / 8;
    int remainder = k % 8;

    for (auto i = 0; i < divisor; i++) {
        for (auto j = 0; j < (8 - 1); j++) {
            update_out(tmp);
            tmp <<= 1;
        }
        update_out(tmp);
        d_k_last_out.push_back(tmp);
        tmp = 0;
    }
    for (auto i = 0; i < remainder - 1; i++) {
        update_out(tmp);
        tmp <<= 1;
    }
    if (remainder) {
        update_out(tmp);
        d_k_last_out.push_back(tmp);
    }

    return d_k_last_out;
}

std::vector<uint8_t>
lfsr::generate_maximal_length()
{
    return next_k(d_maximal_length);
}

uint8_t
lfsr::next_xor_stage(uint8_t stage)
{
    uint8_t return_bit = 0;
    uint32_t tmp_xor = 0;
    tmp_xor = d_taps & d_lfsr;
    tmp_xor = odd_parity(tmp_xor);
    return_bit |= (d_lfsr & 1) ^ (d_lfsr & (1 << (stage - 1)));
    d_last_out = return_bit;
    d_lfsr >>= 1;
    d_lfsr |= (tmp_xor << (d_stages - 1));

    // print_lfsr_state();

    return return_bit;
}

std::vector<uint8_t>
lfsr::next_k_xor_stage(int k, uint8_t stage)
{    uint8_t tmp = 0;
    int divisor = k / 8;
    int remainder = k % 8;

    for (auto i = 0; i < divisor; i++) {
        for (auto j = 0; j < (8 - 1); j++) {
            update_out(tmp);
            tmp <<= 1;
        }
        update_out(tmp);
        d_k_last_out.push_back(tmp);
        tmp = 0;
    }
    for (auto i = 0; i < remainder - 1; i++) {
        update_out(tmp);
        tmp <<= 1;
    }
    if (remainder) {
        update_out(tmp);
        d_k_last_out.push_back(tmp);
    }

    return d_k_last_out;
}

std::vector<uint8_t>
lfsr::generate_maximal_length_xor_stage(uint8_t stage)
{
    return next_k_xor_stage(d_maximal_length, stage);
}

uint32_t
lfsr::validate_taps(std::string taps)
{
    uint32_t tmp_taps = std::stoul(taps, 0, 2);
    if (!(tmp_taps & 1)) {
        std::cout << "\033[1;31mERROR: Taps are wrong!\033[0m\n";
    }
    return tmp_taps;
}

uint8_t
lfsr::get_stages(void) const
{
    return d_stages;
}

uint32_t
lfsr::get_maximal_length(void) const
{
    return d_maximal_length;
}

void
lfsr::print_lfsr_state(void)
{
    std::cout << "lfsr: " << std::bitset<8> (d_lfsr) << std::endl;
    std::cout << "last out: " << +d_last_out << std::endl;
}

void
lfsr::print_all(void)
{
    std::cout << "taps: " << std::bitset<32> (d_taps) << std::endl;
    std::cout << "lfsr: " << std::bitset<32> (d_lfsr) << std::endl;
    std::cout << "last out: " << +d_last_out << std::endl;
    std::cout << "lfsr stages: " << +d_stages << std::endl;
    std::cout << "number of taps: " << +d_number_taps << std::endl;
}

uint8_t
lfsr::count_bits(uint16_t number)
{
    uint8_t bits = 0;
    while (number > 0) {
        bits += number & 1;
        number >>= 1;
    }
    return bits;
}

uint8_t
lfsr::odd_parity(uint8_t number)
{
    number ^= number >> 4;
    number ^= number >> 2;
    number ^= number >> 1;
    return number & 1;
}

void
lfsr::update_out(uint8_t &out)
{
    next();
    out |= d_last_out;
}

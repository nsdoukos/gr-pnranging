INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_PNRANGING pnranging)

FIND_PATH(
    PNRANGING_INCLUDE_DIRS
    NAMES pnranging/api.h
    HINTS $ENV{PNRANGING_DIR}/include
        ${PC_PNRANGING_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    PNRANGING_LIBRARIES
    NAMES gnuradio-pnranging
    HINTS $ENV{PNRANGING_DIR}/lib
        ${PC_PNRANGING_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/pnrangingTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PNRANGING DEFAULT_MSG PNRANGING_LIBRARIES PNRANGING_INCLUDE_DIRS)
MARK_AS_ADVANCED(PNRANGING_LIBRARIES PNRANGING_INCLUDE_DIRS)
